(function () {
    angular
        .module("GroceryApp")
        .controller("GroceryCtrl", GroceryCtrl);

    GroceryCtrl.$inject = ["$http"];

    function GroceryCtrl($http) {
        var vm = this;

        vm.editStatus = false;
        vm.groceries = [];
        vm.brand = "";
        vm.name = "";
        vm.grocery ={
            id: "1",
            upc12: "",
            brand: "",
            name: ""
        };
        vm.inputInvalid = false;
        vm.status = {
            message: "",
            code: 0
        };

        vm.style = {
            "background-color": "white"
        };

        vm.select  = function ($index) {
            vm.selected = $index;
        };

        function slectClicked(id) {
            for (var i = 0; i < vm.groceries.length; i++){
                if (vm.groceries[i].id === Number(id)){
                    vm.grocery = vm.groceries[i];
                    break;
                }
            }
        }

        vm.showDetails = function(id){
            slectClicked(id);                               // retrive the object from array
            vm.editStatus = true;
        };

        vm.resetEntry=function(){
            vm.grocery.id = "";
            vm.grocery.upc12 = "";
            vm.grocery.brand = "";
            vm.grocery.name = "";
        };
        vm.displayEntry = function(){
            vm.resetEntry();
            vm.editStatus = false;
        };


        vm.updateEntry = function(){
            console.log("-- updateEntry()");
            $http
                .put("/api/groceries/" + vm.grocery.id, vm.grocery)
                .then(function (resp){
                    console.log("update successfully");
                    console.log(resp);
                    vm.editStatus = false;
                    vm.resetEntry();
                    // issue search command to refresh the Grocery List
                    vm.search();
                })
                .catch(function(err){
                    console.log("update failed");
                    console.log(err);
                });
        };

        vm.cancel = function(){
            vm.editStatus = false;
            vm.resetEntry();
        };

        vm.deleteClicked = function(id) {
            slectClicked(id);
            console.log("-- deleteClicked() " + id);
            console.log("Grocery " + vm.grocery.name + " is to be deleted.");
        };

        vm.deleteGrocery = function(){
            console.log("-- deleteGrocery()");
            $http
                .delete("/api/groceries/" + vm.grocery.id)
                .then(function () {
                    console.log("Successfully deleted");
                })
                .catch(function () {
                    console.log("Delete fail");
                })
        };

        vm.findByBrand = function (){
            $http
                .get("/api/groceries/brand/" + vm.brand)
                .then(function (response) {
                    console.log("find by brand done ", response.data);
                    vm.groceries = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        };

        vm.findByName = function (){
            $http
                .get("/api/groceries/name/" + vm.name)
                .then(function (response) {
                    console.log("find by name done " + JSON.stringify(response.data));
                    vm.groceries = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        };

        vm.search = function (){
            vm.inputInvalid = false;
            console.log(vm.brand + "" + vm.name);
            if (vm.brand && vm.name){
                $http
                    .get("/api/groceries/" + vm.brand + "/" + vm.name)
                    .then(function (response) {
                        console.log("find " + JSON.stringify(response.data));
                        vm.groceries = response.data;
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }else if(vm.brand){
                vm.findByBrand();
            }else if(vm.name){
                vm.findByName();
            }else{
                vm.status.message = "Both input fields are empty, unable to search!!!";
                vm.code = 400;
                vm.inputInvalid = true;
            }
        };
    }

})();