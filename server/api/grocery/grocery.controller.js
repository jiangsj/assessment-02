var Grocery = require("../../database").Grocery;


exports.listByBrand = function(req,res){
    console.log("reached database >> list by brand");
    console.log(req.params.brand);
    Grocery
        .findAll({
            where: {
                brand: {
                    $like: "%" + req.params.brand + "%"
                }
            },
            limit: 20
        })
        .then(function (result) {
            if (result) {
                console.log(result);
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.listByName = function(req,res){
    console.log("reached database >> list by name");
    console.log(req.params);
    Grocery
        .findAll({
            where: {
                name: {
                    $like: "%" + req.params.name + "%"
                }
            },
            limit: 20
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.listByBrandAndName = function(req,res){
    console.log("reached database >> list 1st 20 contacts");
    console.log(req.params);
    Grocery
        .findAll({
            where: {
                brand: {
                    $like: "%" + req.params.brand + "%"
                },
                name: {
                    $like: "%" + req.params.name + "%"
                }
            },
            limit: 20
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.delete = function(req,res){
    console.log("--reached server--delete");
    console.log(req.params);
    Grocery
        .destroy({
            where: {
                id: req.params.groceryId
            }

        })
        .then(function(result) {
            console.log("deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res){
    console.log("--reached server--update");
    console.log(req.params);
    Grocery
        .find({
            where: {
                id: Number(req.params.groceryId)
            }
        })
        .then(function(response){
            console.log('DB Response', response);
            response.updateAttributes({
                upc12: req.body.upc12,
                brand: req.body.brand,
                name: req.body.name
             }).then(function (){
                console.log("update done");
                res.status(200).end();
            }).catch(function (){
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });

};
